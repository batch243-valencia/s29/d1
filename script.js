db.users.find({age:{$gt:50}}).pretty()

db.users.find({age:{$gte:21}}).pretty()


db.users.find({age:{$lt:50}}).pretty();
db.users.find({age:{$lte:50}}).pretty();
/*
#ne operator
    allows us to find documents that have field number value not equal to a specified value
*/
    
    // db.collectionName.find({field:{$ne:value}});
    db.users.find({age:{$ne:82}}).pretty();

    db.users.find({lastname:{$in:['Doe','Hawking', 'test']}}).pretty();

    db.users.find({courses:["CSS","JavaScript","Python",]});


    db.users.find({age:{$gte:65}}).pretty();

    // db.collectionName.find({$or: [{fieldA: valueA},{fieldB:valueB}]})
  db.users.find(
    {$or:
        [
            {firstName:"Neil"},
            {age:25}
        ]
    }).pretty();

    db.users.find(
    {$or:
        [
            {firstName:"Neil"},
            {age:{$gt:30}}
        
        ]
    }).pretty();
// and
db.users.find({'deparment':"HR",'deparment':"HR"});
    db.users.find(
    {$and:
        [
            {firstName:"Neil"},
            {age:{$gt:30}}
        
        ]
    }).pretty();


    // inclusion 1:true
db.users.find({firstName: "Jane"},
{
    firstName:1,
    lastName:1,
    age:1
})

    // exclusion 0 : false
db.users.find({firstName: "Jane"},
    {        
        department:0,
        contact:0
})


db.users.find({firstName:"Jane"},{'contact.phone':1})

// regEX

db.users.find({firstName: {$regex: 'N'}});
db.users.find({
    firstName:{ $regex:'N',$options:'$i'}})

db.users.updateOne({age:{$lte:17}},
    {
        $set:{
            firstName: "Chris",
            lastName:"Mortel"            
        }

    })


db.users.deleteOne({$and:[{firstName:"Chris"},{lastName:"Mortel"}]})

db.users.deleteOne({$})